﻿using System;
using System.Windows.Forms;
using CifradoAES;

namespace EncriptaTexto
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string user, passwd;
            
            CifrarAES textoCifrar = new CifrarAES();

            textBox2.Text = textoCifrar.cifrarTexto(textBox1.Text.ToString().Trim(), txtBoxLlave.Text, txtBoxLlave.Text, comboBox1.SelectedItem.ToString(), 22,
                                    "1234567891234567", 256);
            //user = textoCifrar.cifrarTexto(textBox1.Text.ToString().Trim(), "Secur3pa$$", "Secur3pa$$", "SHA1", 22,
              //                             "1234567891234567", 256);
            //passwd = textoCifrar.cifrarTexto(textBox2.Text.ToString().Trim(), "Secur3pa$$", "Secur3pa$$"", "SHA1", 22,
              //                               "1234567891234567", 256);
            
            /*string path;
            string pathString;
            string fileName = "Config.conf";

            path = Path.GetDirectoryName(Application.ExecutablePath);

            pathString = System.IO.Path.Combine(path, fileName);

            if (!System.IO.File.Exists(pathString))
            {
                using (StreamWriter f = File.CreateText(pathString))
                {
                    string[] lines = { user, passwd };
                    f.WriteLine(lines[0]);
                    f.WriteLine(lines[1]);
                    //System.IO.File.WriteAllLines(@pathString, lines);
                    f.Close();
                }
            }
            else
            {
                return;
            }*/

        }

        private void button2_Click(object sender, EventArgs e)
        {

            CifrarAES textoDesCifrar = new CifrarAES();

            textBox3.Text = textoDesCifrar.descifrarTextoAES(textBox2.Text.ToString().Trim(), txtBoxLlave.Text, txtBoxLlave.Text, comboBox1.SelectedItem.ToString(), 22,
                                    "1234567891234567", 256);

            /*
            string path;
            string pathString;
            string fileName = "Config.conf";
            string line;

            path = Path.GetDirectoryName(Application.ExecutablePath);

            pathString = System.IO.Path.Combine(path, fileName);

            if (System.IO.File.Exists(pathString))
            {
                using (StreamReader f = new StreamReader(pathString))
                {
                    line = f.ReadLine();
                    textBox3.Text = textoDesCifrar.descifrarTextoAES(line, "Secur3pa$$", "Secur3pa$$", "SHA1", 22,
                                                                     "1234567891234567", 256);
                    line = f.ReadLine();
                    textBox4.Text = textoDesCifrar.descifrarTextoAES(line, "Secur3pa$$", "Secur3pa$$", "SHA1", 22,
                                                                     "1234567891234567", 256);
                    f.Close();
                }
            }
            else
            {
                return;
            }*/
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
